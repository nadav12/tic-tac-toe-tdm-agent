import numpy as np
import helper
import temporal_difference_method as tdm
import gym
import gym_tictactoe



def TDM_vs_TDM(value_state_table_1, value_state_table_2, env):

    """

        Function trains two TDM agents against each other in tic tac toe,
        X and O play is randomely chosen between the two as to not create bias
        during training process.

        INPUT:  value_state_table_1 - The first agent's value state table
                value_state_table_2 - The second agent's value state table
                env - the tic tac toe gym enviroment
        
        OUTPUT: value_state_table_1 - the updated first agent's value state table
                value_state_table_2 - the updated second agent's value state table

    """

    epsilon = 0.1
    alpha = 0.2
    player = helper.X
    done = False
    board = env.reset()
   
    prev_state_X, prev_state_O = list(board), list(board)

    shuffle = np.random.choice([helper.X,helper.O], p=[0.5, 0.5])

    if shuffle == helper.X:
        value_state_table_X = dict(value_state_table_1)
        value_state_table_O = dict(value_state_table_2)
    else:
        value_state_table_X = dict(value_state_table_2)
        value_state_table_O = dict(value_state_table_1)

    while(not done):

        if player == helper.X:
            print("--~ Board ~--\n")
            env.render(mode=None)
            print("~-- Player X's turn --~\n")
            possible_states = list(reversed(helper.possible_states(helper.board_to_state(board), helper.X)))
            a, greed = tdm.policy_function(possible_states, value_state_table_X, epsilon)
            a = helper.move_to_state(board, helper.state_to_board(a))
            board, reward_X, done, infos = env.step(a, helper.X)

        elif player == helper.O:
            print("--~ Board ~--\n")
            env.render(mode=None)
            print("~-- Player O's turn --~\n")
            possible_states = list(reversed(helper.possible_states(helper.board_to_state(board), helper.O)))
            a, greed = tdm.policy_function(possible_states, value_state_table_O, epsilon)
            a = helper.move_to_state(board, helper.state_to_board(a))
            board, reward_O, done, infos = env.step(a, helper.O)

        if not done:
                
            if player == helper.X:
                if greed:
                    value_state_table_X = tdm.value_function(value_state_table_X, helper.board_to_state(prev_state_X), helper.board_to_state(board), alpha)
                prev_state_X = list(board)
                player = helper.O
            elif player == helper.O:
                if greed:
                    value_state_table_O = tdm.value_function(value_state_table_O, helper.board_to_state(prev_state_O), helper.board_to_state(board), alpha)
                prev_state_O = list(board)
                player = helper.X

        else:

            value_state_table_X = tdm.value_function(value_state_table_X, helper.board_to_state(prev_state_X), helper.board_to_state(board), alpha, reward_X, 0)
            value_state_table_O = tdm.value_function(value_state_table_O, helper.board_to_state(prev_state_O), helper.board_to_state(board), alpha, reward_O, 0)
           
            if reward_X == helper.WIN:
                
                print("--~ Board ~--\n")
                env.render(mode=None)
                print("--~* Player X won *~--\n")
                value_state_table_X["victory_counter"] += 1

            elif reward_O == helper.WIN:
                print("--~ Board ~--\n")
                env.render(mode=None)
                print("--~* Player O won *~--\n")
                value_state_table_O["victory_counter"] += 1
            else:
                print("--~ Board ~--\n")
                env.render(mode=None)
                print("--~* Draw *~--\n")

    if shuffle == helper.X:
        value_state_table_1 = value_state_table_X
        value_state_table_2 = value_state_table_O
    else:
        value_state_table_1 = value_state_table_O
        value_state_table_2 = value_state_table_X


    return value_state_table_1, value_state_table_2
        



def main():

    # Trains 5 agents against eachother, after every game the board is updated, agent's saved after every batch of 100 playthroughs

    env = gym.make('TicTacToe-v1', symbols=[1, 2], board_size=helper.BOARD_SIZE, win_size=3) 

    value_state_table_1 = value_state_table_2 = value_state_table_3 = value_state_table_4 = value_state_table_5 = tdm.load_agent("best_agent.txt")

    agents = [value_state_table_1, value_state_table_2, value_state_table_3, value_state_table_4, value_state_table_5]
    counters = [value_state_table_1["victory_counter"], value_state_table_2["victory_counter"], value_state_table_3["victory_counter"], value_state_table_4["victory_counter"]]

    try:

        for epoch in range(10):
            for t in range(100):

                value_state_table_1, value_state_table_2 = TDM_vs_TDM(value_state_table_1, value_state_table_2, env)
                value_state_table_1, value_state_table_3 = TDM_vs_TDM(value_state_table_1, value_state_table_3, env)
                value_state_table_1, value_state_table_4 = TDM_vs_TDM(value_state_table_1, value_state_table_4, env)
                value_state_table_1, value_state_table_5 = TDM_vs_TDM(value_state_table_1, value_state_table_5, env)
                value_state_table_2, value_state_table_3 = TDM_vs_TDM(value_state_table_2, value_state_table_3, env)
                value_state_table_2, value_state_table_4 = TDM_vs_TDM(value_state_table_2, value_state_table_4, env)
                value_state_table_2, value_state_table_5 = TDM_vs_TDM(value_state_table_2, value_state_table_5, env)
                value_state_table_3, value_state_table_4 = TDM_vs_TDM(value_state_table_3, value_state_table_4, env)
                value_state_table_3, value_state_table_5 = TDM_vs_TDM(value_state_table_3, value_state_table_5, env)
                value_state_table_4, value_state_table_5 = TDM_vs_TDM(value_state_table_4, value_state_table_5, env)
                

            for agent in agents: agent = agents[np.argmax(counters)]
            tdm.save_agent(agents[np.argmax(counters)], "best_agent.txt")

    except:

        for agent in agents: agent = agents[np.argmax(counters)]
        tdm.save_agent(agents[np.argmax(counters)], "best_agent.txt")
        
    

   

if __name__ == "__main__":
    main()


