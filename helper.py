import numpy as np

# Helper functions for tic tac toe enviroment

BOARD_SIZE = 3
X = 1
O = 2
DRAW = 0
WIN = 1
IN_GAME = -1
GAMES = 100
STATE_NUM = 19683
TERMINAL_STATE_NUM = 11093


def board_to_state(board):

    """

        Converts board list to state id

        INPUT:  board - the board state
        
        OUTPUT: state - state id

    """

    state = 0

    for cell in range(len(board)):

        if board[cell] != 0:
            value = board[cell]
            state += (value * (3**cell))

    return state



def state_to_board(state_id):

    """

        Converts state id to board list

        INPUT:  state_id - the state's id

        OUTPUT: board - the board state for the id

    """

    board = [0,0,0,0,0,0,0,0,0]
    state = state_id

    for cell in range(len(board)-1,-1,-1):

        value = int(state / (3**cell))
        if value != 0: board[cell] = value
        state -= (value * (3**cell))

    return board

def possible_states(state, player):

    """

        Finds all possible future states given a non terminal state using neat algorithm found online

        INPUT:  state - the current state
                player - current active player

        OUTPUT: possible_future_states - all possible future states 

    """

    board = [0,0,0,0,0,0,0,0,0]
    curr_state = state
    possible_future_states = []

    for cell in range(len(board)-1,-1,-1):
        
        value = int(state / (3**cell))
        if value == 0:

            possible_state = curr_state + (player * (3**cell))
            possible_future_states.append(possible_state)
        state -= (value * (3**cell))

    return possible_future_states

def win_check_X(board):

    """

        Determines if X player won, lost or didnt either (obviously a loss for X is a win for O and vice versa)

        INPUT:  board - the current board state

        OUTPUT: won - whether the X player won, lost or none

    """

    board = np.array(board).reshape(3,3)
    won = None

    if np.all(board[0] == X) or np.all(board[1] == X) or np.all(board[2] == X):
        won = True

    elif np.all(board[0] == O) or np.all(board[1] == O) or np.all(board[2] == O):
        won = False
    
    elif np.all(board.transpose()[0] == X) or np.all(board.transpose()[1] == X) or np.all(board.transpose()[2] == X):
        won = True

    elif np.all(board.transpose()[0] == O) or np.all(board.transpose()[1] == O) or np.all(board.transpose()[2] == O):
        won = False

    elif np.all(np.diag(board) == X) or np.all(np.diag(np.fliplr(board)) == X):
        won = True

    elif np.all(np.diag(board) == O) or np.all(np.diag(np.fliplr(board)) == O):
        won = False
    
    elif np.count_nonzero(board == 0) == 0:
        won = False
        
    return won


def split_states():

    """

        Iterates through the number of legal tic tac toe states, converts each number/id to board state (using neat algorithm found online)
        and then splits states into loss, win and non terminal states (including ties)

        INPUT: None

        OUTPUT: all X_loss_states - loss states for X player
                all X_win_states - win states for X player
                all non_terminal_states in the game

    """

    X_loss_states = []
    X_win_states = []
    non_terminal_states = []

    for state in range(STATE_NUM):

        if win_check_X(state_to_board(state)):
            X_win_states.append(state)
        elif win_check_X(state_to_board(state)) == None:
            non_terminal_states.append(state)
        else:
            X_loss_states.append(state)
    
    return X_loss_states, X_win_states, non_terminal_states

def move_to_state(prev_board, curr_board):

    """

        finds the cell changed in the chosen next state

        INPUT:  prev_board - the previous board state to compare with
                curr_board - the current new board state

        OUTPUT: changed cell number

    """

    cell = 0

    for i, j in zip(prev_board, curr_board):
        if i != j:
            return cell
        cell += 1
    return -1


    
    
    

def main():
    pass

if __name__ == "__main__":
    main()
