import numpy as np

# code for a general temporal difference agent , regardless of enviroment minutia

def init_state_tables (non_terminal_states, non_terminal_state_val, terminal_states=False, terminal_state_val=-1, win_states=False, win_state_val=1, loss_states=False, loss_state_val=0):

    """

        Initializes the value state table,
        gives user as well freedom to give whatever predisposed knowledge about the values of states

        INPUT:  non_terminal_states - normal intermediate states of the game, not victories or losses or ties, 
                non_terminal_state_val - initial value attributed with all non terminal states
                terminal_states - if to include already within state-val table terminal states or allow agent to add terminal states during training
                terminal_state_val - value to attribute to all terminal states
                win_states - if to include already within state-val table win states or allow agent to add win states during training
                win_state_val - value to attribute to all win states
                loss_states - if to include already within state-val table loss states or allow agent to add loss states during training
                loss_state_val - value to attribute to all loss states

        OUTPUT: An initialized value state table including all pre-known states

    """

    value_state_table = {}
   
    if not win_states and not loss_states and not terminal_states:

        for state in non_terminal_states: value_state_table[state] = non_terminal_state_val

    elif not loss_states and not terminal_states:

        for state in non_terminal_states: value_state_table[state] = non_terminal_state_val
        for state in win_states: value_state_table[state] = win_state_val

    elif not win_states and not terminal_states:

        for state in non_terminal_states: value_state_table[state] = non_terminal_state_val
        for state in loss_states: value_state_table[state] = loss_state_val

    elif not terminal_states:

        for state in non_terminal_states: value_state_table[state] = non_terminal_state_val
        for state in win_states: value_state_table[state] = win_state_val
        for state in loss_states: value_state_table[state] = loss_state_val
    
    else:

        for state in non_terminal_states: value_state_table[state] = non_terminal_state_val
        for state in terminal_states: value_state_table[state] = terminal_state_val

    return value_state_table

def policy_function(possible_future_states, value_state_table, epsilon):
    """

        Where the agent maps the current state to the next state based on values within the state value table.
        finding next possible states is done in a seperate function, as this code is independant of any enviroment.

        INPUT:  possible_future_states - all possible future states provided within list
                value_state_table - agent's value state table
                epsilon - likelyhood of agent to go for best state among possible states or explore a random state among possible states

        OUTPUT: new_state - the chosen new state
                greed - whether agent went for best state or tried random state

    """

    greed = np.random.choice(2, p=[epsilon, 1-epsilon])

    if greed:

        print("- NO EXPLORATION -")
        
        max_val = np.max([value_state_table[state] for state in possible_future_states])


        print("- VALUES FOR POSSIBLE STAATES - {}\n- MAX VALUE - {}\n".format([value_state_table[state] for state in possible_future_states], max_val))

        if([value_state_table[state] for state in possible_future_states].count(max_val) != 1):


            print("- MORE THAN ONE MAXIMUM VALUES -\n")

            possible_future_states = [state for state in possible_future_states if value_state_table[state] == max_val]

            new_state = possible_future_states[np.random.randint(0, len(possible_future_states))]

            print("- ALL STATES WITH MAXIMUM VALUE - {}\n- CHOSEN STATE - {}\n".format(possible_future_states, new_state))

        else: 

            print("- ONE MAXIMUM VALUE -\n")

            new_state = [state for state in possible_future_states if value_state_table[state] == max_val][0]

            print("- CHOSEN STATE -", new_state)

    else:

        print("EXPLORED")
        new_state = possible_future_states[np.random.randint(0, len(possible_future_states))]

        print("- CHOSEN STATE -", new_state)

    return new_state, greed

def value_function(value_state_table, prev_state, curr_state, alpha, reward=0, discount=1):
    """

        Updates value of previous state based on value of current state (explained in TEMPORAL_DIFFERENCE_ALG.png)

        INPUT:  value_state_table - the agent's value state table
                prev_state - the previous state of the enviroment
                curr_state - the current state of the enviroment
                alpha - the agent's learning rate (explained in TEMPORAL_DIFFERENCE_ALG.png)
                reward - reward given for the current state
                discount - discount update value (explained in TEMPORAL_DIFFERENCE_ALG.png)

        OUTPUT: the agent's value state table

    """

    value_state_table[prev_state] = value_state_table[prev_state] + alpha * (reward + discount*value_state_table[curr_state] - value_state_table[prev_state])

    return value_state_table

def save_agent(value_state_table, path):

    """

        Saves a TDM agent's value state table to text file

        INPUT:  value_state_table - the agent's value state table
                path - file path

        OUTPUT: None

    """

    agent_file = open(path, "w")
    for state, value in value_state_table.items():
        agent_file.write(str(state) + ' ' + str(value) + '\n')
    agent_file.write('\n')
    
    agent_file.close()

def load_agent(path):

    """

        Loads up a TDM agent's value state table to dictionary

        INPUT:  path - file path

        OUTPUT: agent's value state table

    """

    value_state_table = {'victory_counter':0}
    
    with open(path, "r") as agent_file:
        for line in agent_file:
            value_state_table[eval(line[:line.find(" ")])] = eval(line[line.find(" ")+1:line.find("\n")])
        else:
            pass
            
       
    return value_state_table


def main():

    pass

if __name__ == "__main__":
    main()






