# Tic Tac Toe TDM Agent

A temporal difference method agent capable of playing the game Tic Tac Toe. 
Project was made throughout the summer of 2020, only recently added to GitLab. 
Full explanation of algorithm in repository - TEMPORAL_DIFFERENCE_ALG.png .